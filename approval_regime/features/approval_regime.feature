Feature: Approval regime
  For this example I assume that the student is enrolled in only one subject.
  Tasks can be multiple but the group project is only one.

  Background:
    Given the student Juan exists
    Given the individual task 1 ta exists
    Given the individual task 2 tb exists
    Given the individual task 3 tc exists
    Given the group project pg exists

  
  Scenario: Approved student
    Given the student Juan
    When approves all of the individual tasks
    And assists to 75% of the classes
    And approves 3 iterations of the group project
    Then Juan has approved

  
  Scenario: Disapproved student because of inassistance
    Given the student Juan
    When approves all of the individual tasks
    And assists to 60% of the classes
    And approves 3 iterations of the group project
    Then Juan has not approved

  
  Scenario: Disapproved student because of individual tasks
    Given the student Juan
    When disapproves the individual task tc
    Then Juan has not approved


  Scenario: Disapproved student because of group project
    Given the student Juan
    When approves all of the individual tasks
    And assists to 75% of the classes
    And approves 2 iterations of the group project
    Then Juan has not approved
