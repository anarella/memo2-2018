Given(/^the student (\w+) exists$/) do |student_name|
  @students = {}
  @students[student_name] = Student.new(student_name)
end

Given(/^the individual task 1 (\w+) exists$/) do |task_name|
  @task1 = Task.new(task_name)
end

Given(/^the individual task 2 (\w+) exists$/) do |task_name|
  @task2 = Task.new(task_name)
end

Given(/^the individual task 3 (\w+) exists$/) do |task_name|
  @task3 = Task.new(task_name)
end

Given(/^the group project (\w+) exists$/) do |group_project_name|
  @group_project = GroupProject.new(group_project_name)
end

Given(/^the student (\w+)$/) do |student_name|
  @student = @students.fetch(student_name)
end

When(/^approves all of the individual tasks$/) do
  @student.assign_task(@task1)
  @student.assign_task(@task2)
  @student.assign_task(@task3)
  tasks = @student.tasks
  tasks.each do |task_name, task|
    task.approve
  end
end

When(/^assists to (\d+)% of the classes$/) do |percentage|
  @student.assistance = percentage.to_i / 100.0
end

When(/^approves (\d+) iterations of the group project$/) do |number_of_iterations|
  @student.group_project = @group_project
  group_project = @student.group_project
  group_project.approve_iterations(number_of_iterations.to_i)
end

Then(/^(\w+) has approved$/) do |student_name|
  @result = @student.approved?
  expect(@result).to be true
end

Then(/^(\w+) has not approved$/) do |student_name|
  @result = @student.approved?
  expect(@result).to be false
end

When(/^disapproves the individual task (\w+)$/) do |task_name|
  # Task is originally disapproved when created
end
