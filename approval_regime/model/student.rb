class Student
  
  # Minimum assistance required to approve
  MIN_ASSISTANCE = 0.75
  
  # Minimum of iterations of the group project required to approve
  MIN_ITERATIONS = 3

  def initialize(name)
    @name = name
    @tasks = {}
    @assistance = 0
    @approved = false
  end
  
  def name
    @name
  end
  
  def tasks
    @tasks
  end

  def group_project
    @group_project
  end
  
  def assistance
    @assistance
  end

  def assistance=(percentage)
    @assistance = percentage
  end
    
  def assign_task(task)
    @tasks[task.name] = task
  end

  def group_project=(group_project)
    @group_project = group_project
  end
  
  def approved?
    check_approval_state
    @approved
  end
  
  private
  
  def check_approval_state
    still_one_task_disapproved = false
    @tasks.each do |task_name, task|
      if !task.approved?
        still_one_task_disapproved = true
      end
    end
    
    group_project_approved = true
    if @group_project
      if @group_project.approved_iterations < MIN_ITERATIONS
        group_project_approved = false
      end
    end
    
    if !still_one_task_disapproved && @assistance >= MIN_ASSISTANCE && group_project_approved
      @approved = true
    end
  end

end
