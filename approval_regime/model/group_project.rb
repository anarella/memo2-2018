class GroupProject

  def initialize(name)
    @name = name
    @approved_iterations = 0
  end
  
  def name
    @name
  end
  
  def approved_iterations
    @approved_iterations
  end

  def approve_iterations(number)
    @approved_iterations = number
  end
end
