class Task

  def initialize(name)
    @name = name
    @approved = false
  end
  
  def name
    @name
  end
  
  def approved?
    @approved
  end
  
  def approve
    @approved = true
  end

end
