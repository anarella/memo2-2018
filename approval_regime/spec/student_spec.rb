require 'rspec' 
require_relative '../model/student'

describe 'Student' do
  
  let(:name) { "Pedro" }
  let(:student) { Student.new(name) }  
  let(:task_name) { "Task A" }
  let(:group_project_name) { "Trabajo Profesional" }
   
  it 'name of student should be "Pedro"' do
    expect(student.name).to eq "Pedro"
  end
  
  it 'student has task "Task A" assigned' do
    task = Task.new(task_name)
    student.assign_task(task)
    expect(student.tasks.fetch(task_name).name).to eq "Task A"
  end

  it 'student has group project "Trabajo Profesional" assigned' do
    group_project = GroupProject.new(group_project_name)
    student.group_project = group_project
    expect(student.group_project.name).to eq "Trabajo Profesional"
  end
  
  it 'assistance should be zero when created' do
    expect(student.assistance).to eq 0
  end
  
  it 'assitance should be 0.8 after set' do
    student.assistance = 0.8
    expect(student.assistance).to eq 0.8
  end
  
  it 'student should not be approved when created' do
    expect(student.approved?).to eq false
  end
  
  it 'student should be approved' do
    task_1 = Task.new("T1")
    task_2 = Task.new("T2")
    task_3 = Task.new("T3")
    student.assign_task(task_1)
    student.assign_task(task_2)
    student.assign_task(task_3)
    task_1.approve
    task_2.approve
    task_3.approve
    group_project = GroupProject.new("GP")
    student.group_project = group_project
    group_project.approve_iterations(3)
    student.assistance = 0.75
    expect(student.approved?).to eq true
  end
    
end
