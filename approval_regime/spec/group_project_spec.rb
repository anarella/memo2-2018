require 'rspec' 
require_relative '../model/group_project'

describe 'Group Project' do
  
  let(:name) { "Trabajo Profesional" }
  let(:group_project) { GroupProject.new(name) }  
   
  it 'name of group project should be "Trabajo Profesional"' do
    expect(group_project.name).to eq "Trabajo Profesional"
  end
  
  it 'number of iterations approved should be 0 when created' do
    expect(group_project.approved_iterations).to eq 0
  end
  
  it 'number of iterations approved should be 5' do
    group_project.approve_iterations(5)
    expect(group_project.approved_iterations).to eq 5
  end

end
