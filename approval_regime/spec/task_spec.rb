require 'rspec' 
require_relative '../model/task'

describe 'Task' do
  
  let(:name) { "Task A" }
  let(:task) { Task.new(name) }  
   
  it 'name of task should be "Task A"' do
    expect(task.name).to eq "Task A"
  end
  
  it 'task should be disapproved when created' do
    expect(task.approved?).to eq false
  end
  
  it 'task should be approved' do
    task.approve
    expect(task.approved?).to eq true
  end

end
