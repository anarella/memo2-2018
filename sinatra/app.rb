require 'sinatra'
require 'json'
require_relative 'model/chopper'

get '/sum' do
  chopper = Chopper.new
  strings_array = params["x"].split(",")
  numbers = []
  strings_array.each do |element|
    numbers.push(element.to_i)
  end
  content_type :json
  { :sum => params["x"], :resultado => chopper.sum(numbers)}.to_json
end

post '/chop' do
  chopper = Chopper.new
  x = params["x"].to_i
  strings_array = params["y"].split(",")
  numbers = []
  strings_array.each do |element|
    numbers.push(element.to_i)
  end
  content_type :json
  { :chop => "x=#{params["x"]}, y=#{params["y"]}", :resultado => chopper.chop(x,numbers)}.to_json
end
