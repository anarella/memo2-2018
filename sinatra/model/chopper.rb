class Chopper

  def chop(n, array)
    if !array.empty?
      pos = 0
      array.each do |element|
      
        if element == n
          return pos
        end
        pos = pos + 1
		      
      end
    end
    return -1
    
  end
  
  def sum(array)
    numbers = {
      0 => "cero",
      1 => "uno",
      2 => "dos",
      3 => "tres",
      4 => "cuatro",
      5 => "cinco",
      6 => "seis",
      7 => "siete",
      8 => "ocho",
      9 => "nueve"
    }
    string = ""
    
    if !array.empty?
      sum = 0
      array.each do |element|
        sum = sum + element
      end
      digits = sum.divmod(10)
      if digits.at(0).abs < 10
        if digits.at(0).abs != 0
          string = numbers.fetch(digits.at(0).abs) + ","
        end
        string = string + numbers.fetch(digits.at(1).abs)
      else
        string = "demasiado grande"
      end
    else
      string = "vacio"
    end
    return string
    
  end

end
