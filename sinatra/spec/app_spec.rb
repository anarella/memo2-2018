# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "sum de [9,9] debe ser uno,ocho" do
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include 'uno,ocho'
  end
  
  it "chop de 3 y [0,7,3] debe ser 2" do
    post '/chop', {'x':3, 'y':'0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '2'
  end

end
