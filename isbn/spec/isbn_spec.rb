require 'rspec' 
require_relative '../model/isbn'

describe 'ISBN' do

  let(:isbn) { ISBN.new }
  
  it 'valid de string vacio deberia ser false' do
    expect(isbn.valid("")).to eq false
  end

  it 'valid de string con menos de 10 digits deberia ser false' do
    expect(isbn.valid("658472315")).to eq false
  end

  it 'valid de string con mas de 10 digits deberia ser false' do
    expect(isbn.valid("65847231525")).to eq false
  end

  it 'valid de string con check digit correcto deberia ser true' do
    expect(isbn.valid("9871138679")).to eq true
  end

  it 'valid de string con check digit incorrecto deberia ser false' do
    expect(isbn.valid("9871138670")).to eq false
  end

  it 'valid de string con check digit correcto X deberia ser true' do
    expect(isbn.valid("950150008X")).to eq true
  end

  it 'valid de string con letras deberia ser false' do
    expect(isbn.valid("987113867f")).to eq false
  end

  it 'valid de string con menos de 10 digits y guiones deberia ser false' do
    expect(isbn.valid("658-4723-1-5")).to eq false
  end

  it 'valid de string con mas de 10 digits y guiones deberia ser false' do
    expect(isbn.valid("658-4723-15-25")).to eq false
  end

  it 'valid de string con check digit correcto y guiones deberia ser true' do
    expect(isbn.valid("987-1138-67-9")).to eq true
  end

  it 'valid de string con check digit incorrecto y guiones deberia ser false' do
    expect(isbn.valid("987-1138-67-0")).to eq false
  end

  it 'valid de string con check digit correcto X y guiones deberia ser true' do
    expect(isbn.valid("950-1500-08-X")).to eq true
  end

  it 'valid de string con letras y guiones deberia ser false' do
    expect(isbn.valid("987-1138-67-f")).to eq false
  end

  it 'valid de string con menos de 10 digits y espacios deberia ser false' do
    expect(isbn.valid("658 4723 1 5")).to eq false
  end

  it 'valid de string con mas de 10 digits y espacios deberia ser false' do
    expect(isbn.valid("658 4723 15 25")).to eq false
  end

  it 'valid de string con check digit correcto y espacios deberia ser true' do
    expect(isbn.valid("84 8450 863 3")).to eq true
  end

  it 'valid de string con check digit incorrecto y espacios deberia ser false' do
    expect(isbn.valid("84 8450 863 5")).to eq false
  end

  it 'valid de string con check digit correcto X y espacios deberia ser true' do
    expect(isbn.valid("950 1500 08 X")).to eq true
  end

  it 'valid de string con letras y espacios deberia ser false' do
    expect(isbn.valid("987 1138 67 f")).to eq false
  end
    
end
