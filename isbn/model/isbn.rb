# Only ISBN-10 is considered
class ISBN

  MODULE = 11
  STRING_LENGTH = 10
  X_CHECK_DIGIT = "X"
  SEPARATORS = ["-", " "]
  
  def valid(string)
    if string.empty?
      return false
    end
    string_without_separators = condense_string(string)
    if string_without_separators.length != STRING_LENGTH || \
    !validate_string_numbers(string_without_separators)
      return false
    end
    return validate_check_digit(string_without_separators)
  end
    
  def calculate_check_digit(string)
    sum = 0
    pos = 1
    index = 0
    while pos < STRING_LENGTH
      res = pos * string[index].to_i
      sum = sum + res
      index = index + 1
      pos = pos + 1
    end
    return sum.modulo(MODULE)
  end
  
  def validate_check_digit(string)
    check_digit_calc = calculate_check_digit(string)
    if check_digit_calc == STRING_LENGTH && string[9] == X_CHECK_DIGIT
      return true
    end
    if check_digit_calc == string[9].to_i
      return true
    end
    return false
  end
  
  def validate_string_numbers(string)
    digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    pos = 0
    while pos < STRING_LENGTH
      if !digits.include?(string[pos]) && pos != 9 && string[pos] != X_CHECK_DIGIT
        return false
      end
      pos = pos + 1
    end
    return true
  end
  
  def condense_string(string)
    result = ""
    pos = 0
    while pos < string.length
      if !SEPARATORS.include?(string[pos])
        result = result + string[pos]
      end
      pos = pos + 1
    end
    return result
  end
  
end
