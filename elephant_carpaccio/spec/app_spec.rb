# spec/app_spec.rb
require File.expand_path '../spec_helper.rb', __FILE__
require 'json'

describe "Sinatra Application" do

  it "total_price de price=10, quantity=99 y state=HI debe ser 990" do
    get '/total_price', {'price':10, 'quantity':99, 'state':'HI'}
    expect(last_response).to be_ok
    response = JSON.parse(last_response.body)
    expect(response.fetch("result")).to eq(990.0)
  end
  
  it "total_price de price=101, quantity=10.5 y state=HI debe ser 1028.69" do
    get '/total_price', {'price':101, 'quantity':10.5, 'state':'HI'}
    expect(last_response).to be_ok
    response = JSON.parse(last_response.body)
    expect(response.fetch("result")).to eq(1028.69)
  end

  it 'total_price de price=8.5, quantity=100.1 y state=AL deberia ser 884.88' do
    get '/total_price', {'price':8.5, 'quantity':100.1, 'state':'AL'}
    expect(last_response).to be_ok
    response = JSON.parse(last_response.body)
    expect(response.fetch("result")).to eq(884.88)
  end

  it 'total_price de price=60.5, quantity=100.1 y state=TX deberia ser 6112.83' do
    get '/total_price', {'price':60.5, 'quantity':100.1, 'state':'TX'}
    expect(last_response).to be_ok
    response = JSON.parse(last_response.body)
    expect(response.fetch("result")).to eq(6112.83)
  end

  it 'total_price de price negativo deberia dar ERROR 412' do
    get '/total_price', {'price':-60.5, 'quantity':100.1, 'state':'TX'}
    expect(last_response.status).to eq(412)
  end

  it 'total_price de quantity negativa deberia dar ERROR 412' do
    get '/total_price', {'price':60.5, 'quantity':-100.1, 'state':'TX'}
    expect(last_response.status).to eq(412)
  end

end
