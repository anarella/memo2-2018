require 'rspec' 
require_relative '../model/price_calculator'

describe 'Price Calculator' do

  let(:price_calculator) { PriceCalculator.new }  
   
  it 'total_price con enteros sin descuento ni impuestos deberia ser 990' do
    expect(price_calculator.total_price(10,99,'HI')).to eq 990.00
  end
  
  it 'total_price con floats sin descuento ni impuestos deberia ser 850.85' do
    expect(price_calculator.total_price(8.5,100.1,'HI')).to eq 850.85
  end

  it 'total_price con descuento sin impuestos deberia ser 1028.69' do
    expect(price_calculator.total_price(101,10.5,'HI')).to eq 1028.69
  end

  it 'total_price con descuento sin impuestos deberia ser 5015.43' do
    expect(price_calculator.total_price(502.8,10.5,'HI')).to eq 5015.43
  end

  it 'total_price con descuento sin impuestos deberia ser 27171.18' do
    expect(price_calculator.total_price(300.4,100.5,'HI')).to eq 27171.18
  end

  it 'total_price con descuento sin impuestos deberia ser 42712.50' do
    expect(price_calculator.total_price(500,100.5,'HI')).to eq 42712.50
  end

  it 'total_price sin descuento con impuestos deberia ser 884.88' do
    expect(price_calculator.total_price(8.5,100.1,'AL')).to eq 884.88
  end

  it 'total_price con descuento y con impuestos deberia ser 6112.83' do
    expect(price_calculator.total_price(60.5,100.1,'TX')).to eq 6112.83
  end

end
