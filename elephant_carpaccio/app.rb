require 'sinatra'
require 'json'
require_relative 'model/price_calculator'

get '/total_price' do
  price_calculator = PriceCalculator.new
  price = params["price"].to_f
  quantity = params["quantity"].to_f
  if price < 0 || quantity < 0
    error 412
  end
  state = params["state"]
  content_type :json
  { :total_price => "price=#{params["price"]}, quantity=#{params["quantity"]}, state=#{\
  params["state"]}", :result => price_calculator.total_price(price,quantity,state)}.to_json
end
