class PriceCalculator
  
  DISCOUNT_RATES = {
  0.0 => 0.0,
  1000.0 => 0.03,
  5000.0 => 0.05,
  7000.0 => 0.07,
  10000.0 => 0.1,
  50000.0 => 0.15
  }
  
  TAX_RATES = {"UT" => 0.0685, "NV" => 0.08, "TX" => 0.0625, "AL" => 0.04, "CA" => 0.0825}

  def total_price(price, quantity, state)
    order_value = price * quantity
    discount = define_discount(order_value)
    total = order_value * (1 - discount)
    tax = define_tax(state)
    total_price = total * (1 + tax)
    return total_price.round(2)
  end
  
  private
  
  def define_discount(amount)
    minimum = 0.0
    DISCOUNT_RATES.keys.each do |number|
      if amount >= number && number >= minimum
        minimum = number
      end
    end
    return DISCOUNT_RATES.fetch(minimum)
  end
  
  def define_tax(state)
    tax_rate = TAX_RATES.fetch(state, 0.0)
    return tax_rate
  end

end
